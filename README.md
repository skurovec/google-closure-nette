google-closure-nette
====================

Simple ajax library for Nette written in Coffeescript.

All anchors with ajax class are transformed into click and snippet response parsed.


### Install guide

- copy nette.js to your website
- add `<script type="text/javascript" src="path/to/nette.js"></script>`
- append just before ending body tag
	`<script type="text/javascript">
		nette.start();
	</script>`