###*
  @fileoverview Basic Nette ajax support
  @author Jan Skurovec
###

goog.provide 'nette.start'

goog.require 'nette.ajax'

nette.start = () ->
	nette.ajax()

#export nette.start for compilation
goog.exportSymbol 'nette.start', nette.start