###*
  @fileoverview Basic Nette ajax support
  @author Jan Skurovec
###

goog.provide 'nette.ajax'

#requirements
goog.require 'goog.dom'
goog.require 'goog.dom.forms'
goog.require 'goog.Uri'
goog.require 'goog.array'
goog.require 'goog.events'
goog.require 'goog.net.XhrIo'
goog.require 'goog.json'

nette.ajax = () ->
	#listen to click or sbmit on body; live
	goog.events.listen document.body, [goog.events.EventType.CLICK, goog.events.EventType.SUBMIT], (e) ->
		el = e.target
		# but use only events on a.ajax
		if el.tagName.toLowerCase() == 'a' && goog.dom.classes.has el, 'ajax'
			goog.events.Event.preventDefault e
			nette.ajax.linkRequest el.href
		# or form.ajax
		if el.tagName.toLowerCase() == 'form' && goog.dom.classes.has el, 'ajax'
			goog.events.Event.preventDefault e
			nette.ajax.formRequest el

#process response data
nette.ajax.processData = (data) ->
	#to export snippets
	snippets = data['snippets']
	if snippets
		for i of snippets
			el = goog.dom.getElement i
			if el
				el.innerHTML  = snippets[i]

#get request
nette.ajax.linkRequest = (url) ->
	request = new goog.net.XhrIo()
	# Nette uses this header detection in isAjax() method
	request.headers.set('X-Requested-With' ,'XMLHttpRequest');
	request.send url
	goog.events.listen request, 'complete', () ->
		if request.isSuccess()
			nette.ajax.processData goog.json.unsafeParse request.getResponse()


nette.ajax.formRequest = (form) ->
	request = new goog.net.XhrIo()
	# Nette uses this header detection in isAjax() method
	request.headers.set('X-Requested-With' ,'XMLHttpRequest');

	if form.method.toLowerCase() == 'get'
		#get url from FORM action
		getUrl = goog.Uri.parse form.action

		queryString = getUrl.getQueryData()
		queryString.extend goog.Uri.QueryData.createFromMap goog.dom.forms.getFormDataMap form
		
		#join action and form data to one url
		getUrl.setQueryData queryString

		#send GET request with FORM
		request.send getUrl.toString(), form.method
	else
		#send POST request with form data
		request.send form.action, form.method,  goog.dom.forms.getFormDataString form

	#wait for response and process data
	goog.events.listen request, 'complete', () ->
		if request.isSuccess()
			nette.ajax.processData goog.json.unsafeParse request.getResponse()	


#export nette.start for compilation
goog.exportSymbol 'nette.ajax', nette.ajax